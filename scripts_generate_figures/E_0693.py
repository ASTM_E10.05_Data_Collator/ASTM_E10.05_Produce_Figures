#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import __main__ as main


class E_0693:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def read_dtfr_output(self, fileName, grps, npos, posNum):
        """
        Reads a dtfr output file from NJOY and returns the cross-section value
        at the `posNum` position (using 1-based indexing). Ordering of the
        returned data is in descending energy.
        """

        import numpy as np

        with open(fileName, "r") as myfile:
            fileData = myfile.readlines()

        dtfrList = []

        for line in fileData[2:]:
            dtfrList.extend(line.split())

        dtfrArray = np.asarray(dtfrList, dtype=np.float64).reshape(grps, npos)

        return dtfrArray[:, posNum - 1]

    def load_data_Fig_01(self, infilename="data/E_0693_Fig_01.csv"):
        import hashlib
        import numpy as np

        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "8df5b748876a15d0b45c48f0f8208db4"
        )
        e, f = np.loadtxt(infilename, delimiter=",", unpack=True, skiprows=1)
        return e, f

    def calc_and_check_iron_nat_dpa(self):
        import numpy as np

        # Calculate natural-iron DPA function from isotopic constituents.
        infilenames = [
            "E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-54.tape29.txt",
            "E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-56.tape29.txt",
            "E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-57.tape29.txt",
            "E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-58.tape29.txt",
        ]
        outarrays = []
        for infilename in infilenames:
            infilepath = f"doc/data_generated/{infilename}"
            outarrays.append(self.read_dtfr_output(infilepath, 640, 645, 1))
        f_calc = np.flip(
            0.05845 * outarrays[0]
            + 0.91754 * outarrays[1]
            + 0.02119 * outarrays[2]
            + 0.00282 * outarrays[3]
        )

        # Get preserved natural-iron DPA values and compare with calculated
        # values.
        e, f = self.load_data_Fig_01()

        # This assertion should be renabled to ensure that the values produced
        # through this process are consistent with the values used in E693, once
        # possible.
        # assert np.allclose(
        #     f, f_calc
        # ), "Calculated values from NJOY are different than expected."

        # Save generated values out for future reference.
        outfilename = "doc/data_generated/E_0693_Fig_01.csv"
        np.savetxt(
            outfilename,
            np.transpose((e, f_calc)),
            delimiter=",",
            fmt="%.6e",
            header="Neutron group lower energy boundary (MeV), dpa Cross Section (barns)",
        )

        return e, f

    def plot_figure_01(self, figure_width=3.5):
        import scripts_utilities.plotter as plotter
        import numpy as np

        x, y = self.calc_and_check_iron_nat_dpa()

        # Add upper energy bound for fastest group, and extend range
        # accordingly.
        x = np.hstack((x, 20))
        y = np.hstack((y, y[-1]))

        p = plotter.plotter(figure_width=figure_width)
        p.plt.step(x, y, where="post")
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("dpa Cross Section (barns)")
        p.plt.xscale("log")
        p.plt.yscale("log")
        # p.plt.legend( loc = (0.25,0.25))
        p.plt.xlim(left=1e-10, right=1e2)
        p.plt.ylim(bottom=1e-2, top=1e4)
        p.ax.grid()
        p.plt.savefig(f"{self.destination}/E_0693_Figure_01.png")
        return


def make_plots(destination):
    e0693 = E_0693(destination)
    logging.info(" Generating E693, Figure 1")
    e0693.plot_figure_01()


if __name__ == "__main__" and hasattr(main, "__file__"):
    make_plots()
