#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import __main__ as main


class E_0264:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def plot_figure_01(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np
        import matplotlib.ticker as ticker
        from scipy.interpolate import interp1d

        # Import the data
        infilename = "data/E_0264_Fig_01.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "355744e8a35ff66e4c1fa87ac6744508"
        )
        data = np.genfromtxt(infilename, delimiter=",", skip_header=1)
        x = data[:, 0]
        y = np.transpose(data[:, [1, 2, 3, 4]])
        x_smooth = np.linspace(x.min(), x.max(), 1000)

        # Create 2 subplots
        p = plotter.plotter(figure_width=figure_width)
        figure, (ax1, ax2) = p.plt.subplots(
            1,
            2,
            sharey=False,
            facecolor="w",
            gridspec_kw={"width_ratios": [3, 1]},
            figsize=(figure_width, figure_width / 1.61),
        )

        # Smooth and plot each set of data
        for burnup in y:
            y_smooth = interp1d(x, burnup, kind="cubic")
            ax1.plot(
                x_smooth, y_smooth(x_smooth), linestyle="-", color="k", marker="None"
            )
            ax2.plot(
                x_smooth, y_smooth(x_smooth), linestyle="--", color="k", marker="None"
            )

        # Format the y-axis to be different on left and right
        fig1_primary_y_axis = [1.0, 1.2, 1.4, 1.6, 1.8]
        ax1.set_ylabel(
            r"$\frac{\mathrm{No.\ of\ }\ ^{58}\mathrm{Co\ Atoms\ Without\ Burnup}}{\mathrm{No.\ of\ }\ ^{58}\mathrm{Co\ Atoms\ With\ Burnup}}=R$"
        )
        ax1.set_ylim(1.0, 1.8)
        ax1.set_yticks(fig1_primary_y_axis)
        ax1.yaxis.set_major_formatter(ticker.FixedFormatter(fig1_primary_y_axis))
        ax1.yaxis.tick_left()

        fig1_secondary_y_axis = [1.0, 1.2, 1.4, 1.8, 2.1, 2.5, 3.2]
        ax2.set_ylim(1.0, 3.2)
        ax2.set_yticks(fig1_secondary_y_axis)
        ax2.yaxis.set_major_formatter(ticker.FixedFormatter(fig1_secondary_y_axis))
        ax2.yaxis.tick_right()

        p.plt.gca().yaxis.set_minor_formatter(ticker.NullFormatter())

        # Breakup the x-axis between 0-100 and 400-600
        ax1.set_xlim(0, 100)
        ax2.set_xlim(200, 600)
        ax1.spines["right"].set_visible(False)
        ax2.spines["left"].set_visible(False)
        ax1.set_xticks([x for x in range(0, 120, 20)])
        ax2.set_xticks([x for x in range(200, 800, 200)])

        # x-axis label centered
        figure.text(0.5, 0.01, "Exposure Time (hours)", ha="center")

        # Gridlines
        ax1.grid(linestyle="-")
        ax2.grid(linestyle="--")

        # Annotations
        arrow = dict(
            arrowstyle="fancy",
            facecolor="black",
            connectionstyle="arc3,rad=-0.3",
            relpos=(0, 0.5),
        )
        ax1.annotate(
            (r"$\Phi_\mathrm{th} = 5 \times 10^{13}$"),
            xy=(40, 1.088),
            xytext=(56.75, 1.03),
            arrowprops=arrow,
            fontsize=8,
        )
        ax1.annotate(
            (r"$\Phi_\mathrm{th} = 1 \times 10^{14}$"),
            xy=(40, 1.158),
            xytext=(56.75, 1.118),
            arrowprops=arrow,
            fontsize=8,
        )
        ax1.annotate(
            (r"$\Phi_\mathrm{th} = 3 \times 10^{14}$"),
            xy=(40, 1.325),
            xytext=(56.75, 1.25),
            arrowprops=arrow,
            fontsize=8,
        )
        ax1.annotate(
            (r"$\Phi_\mathrm{th} = 6 \times 10^{14}$"),
            xy=(40, 1.462),
            xytext=(56.75, 1.41),
            arrowprops=arrow,
            fontsize=8,
        )

        # Save the figure
        p.plt.savefig(self.destination + "/E_0264_Figure_01.png")
        return

    def plot_figure_02(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np
        from scripts_utilities.retrieve_zvd_data import xs_data_exp_eval
        from scipy.interpolate import interp1d

        infilename = "data/E_0264_Fig_02.zvd"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "bd1a87396557bf683a6d5c6ad28ffd20"
        )
        data = xs_data_exp_eval(infilename)

        x_exp = data.exp.X
        x_err = data.exp.dX
        y_exp = data.exp.Y
        y_err = data.exp.dY
        x_eval = data.eval.X
        y_eval = data.eval.Y

        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x_eval, y_eval, color="#000000", zorder=10, label="Eval. Data")
        p.plt.errorbar(
            x_exp,
            y_exp,
            xerr=x_err,
            yerr=y_err,
            fmt="s",
            markersize=2.0,
            label="Exp. Data",
        )
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Cross Section (barns)")
        p.plt.xscale("linear")
        p.plt.yscale("linear")
        p.plt.xlim(left=0, right=20)
        p.plt.ylim(bottom=0, top=1.0)
        p.plt.xticks([x for x in range(0, 20, 5)])
        p.plt.minorticks_on()
        p.ax.grid()
        p.plt.legend(loc="best", fontsize=8)
        p.plt.savefig(self.destination + "/E_0264_Figure_02.png")

        return

    def plot_figure_03(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = "data/E_0264_Fig_03.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "d6f56db8fd17429bf45ae82a21326d3a"
        )
        data = np.genfromtxt(infilename, delimiter=";", skip_header=4)
        x = np.array(data[:, 0]) / 1e6
        xs = np.array(data[:, 1])
        unc = np.array(data[:, 2])
        y = 100 * np.divide(unc, xs, out=np.zeros_like(xs), where=xs != 0)

        p = plotter.plotter(figure_width=figure_width)
        p.plt.step(x, y)
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Standard Deviation (%)")
        p.plt.xscale("linear")
        p.plt.yscale("linear")
        p.plt.xlim(left=0, right=20)
        p.plt.ylim(bottom=0.0, top=20)
        p.plt.minorticks_on()
        p.plt.xticks([5 * x for x in range(0, 5)])
        p.plt.yticks([5 * y for y in range(0, 5)])
        p.ax.grid()
        p.plt.savefig(self.destination + "/E_0264_Figure_03.png")
        return


def make_plots(destination):
    e0264 = E_0264(destination)
    logging.info(" Generating E264, Figure 1")
    e0264.plot_figure_01()
    logging.info(" Generating E264, Figure 2")
    e0264.plot_figure_02()
    logging.info(" Generating E264, Figure 3")
    e0264.plot_figure_03()


if __name__ == "__main__" and hasattr(main, "__file__"):
    make_plots()
