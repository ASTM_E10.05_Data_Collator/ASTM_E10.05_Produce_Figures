Retrieving EXFOR Data
=====================

This chapter describes how to retrieve EXFOR data using a web-based utility.
These steps are useful to generate plots showing experimental values versus the
agreed upon quantities used in nuclear data libraries.

The EXFOR database is most easily accessed at
https://www-nds.iaea.org/exfor/exfor.htm.  At the time of writing, it appears as
shown in :ref:`fig:EXFOR home page`

The steps necessary to retrieve data, using :superscript:`237`\ Np(n,f)FP as an
example, are:

1. Navigate to https://www-nds.iaea.org/exfor/exfor.htm in a web browser.

2. Enter the search parameters.  For this example, click the checkbox next to
   `Target` and choose `Np` and then `Np-237`.  The Target text field should now
   be populated.  Likewise, click the checkbox next to `Reaction` and choose
   `n,f` (which should also populate the accompanying text field).

3. Click `Submit`.

4. Select the radio button(s) for the data set(s) to be plotted.  For this
   example, select `93-NP-237(N,F),,SIG   C4: MF3 MT18` (corresponding to set
   21).

5. Click the `Quick-plot` checkbox.  See :ref:`fig:Select experimental data sets
   and request plots`

6. Click the `Retrieve` button.  One should now see a plot such as in
   :ref:`fig:Initial plot of evaluated data set(s)`

7. To retrieve ENDF data, click the ENDF button as shown in :ref:`fig:Select
   experimental data sets and request plots`

8. Select the ENDF data set(s) to plot and click the Plot button as shown in
   :ref:`fig:Select ENDF data set(s) to plot`

9. Retrieve the raw data in ZView data format by clicking the `plotted data`
   hyperlink; see :ref:`fig:Resulting ENDF and EXFOR data, available for export`
   The data should appear similar to what appears in :ref:`fig:ZVD-formatted raw
   data set values`

10. Save the resulting ASCII file as a ``.zvd`` file.

.. note::
   The ZVD-formatted raw data file contains the EXFOR data first and the ENDF
   data second.  This can create parsing challenges and all output should be
   thoroughly inspected to ensure that all data sets are correctly represented.

Figures
-------

.. _fig:EXFOR home page:
.. figure:: figures/EXFOR_01.png
   :width: 6.5in

   EXFOR home page.

.. _fig:EXFOR search parameter entry:
.. figure:: figures/EXFOR_02.png
   :width: 6.5in

   EXFOR search parameter entry.

.. _fig:Select experimental data sets and request plots:
.. figure:: figures/EXFOR_03.png
   :width: 6.5in

   Select experimental data sets and request plots.

.. _fig:Initial plot of evaluated data set(s):
.. figure:: figures/EXFOR_04.png
   :width: 6.5in

   Initial plot of evaluated data set(s).

.. _fig:Select ENDF data set(s) to plot:
.. figure:: figures/EXFOR_05.png
   :width: 6.5in

   Select ENDF data set(s) to plot.

.. _fig:Resulting ENDF and EXFOR data, available for export:
.. figure:: figures/EXFOR_06.png
   :width: 6.5in

   Resulting ENDF and EXFOR data, available for export.

.. _fig:ZVD-formatted raw data set values:
.. figure:: figures/EXFOR_07.png
   :width: 6.5in

   ZVD-formatted raw data set values.

