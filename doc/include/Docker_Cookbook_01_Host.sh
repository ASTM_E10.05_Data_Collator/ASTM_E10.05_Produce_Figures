#!/bin/bash

export GIT=$HOME/GIT
docker rm -f run_njoy2016 run_astmplotter
docker pull registry.gitlab.com/astm_e10_data_collator/astm_e10_produce_figures:njoy2016
docker pull registry.gitlab.com/astm_e10_data_collator/astm_e10_produce_figures:astmplotter
docker images --all
mkdir $GIT && cd $GIT
git clone https://gitlab.com/ASTM_E10_Data_Collator/ASTM_E10_Produce_Figures.git
cd ASTM_E10_Produce_Figures
docker create -t --name run_njoy2016 69a3f0d4953b
docker cp data run_njoy2016:/
docker start run_njoy2016
docker exec -it run_njoy2016 bash
