E2956
=====

.. note::
  The figure(s) given herein exist without technical context and for the sole
  purpose of communicating how they are generated.  If one wishes to use the
  figure(s) (and generating processes) herein, he or she must consult the
  associated technical standard, described next.

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 2956.  The recommended citation for this standard is:

* ASTM E2956-14, Standard Guide for Monitoring the Neutron Exposure of LWR
  Reactor Pressure Vessels, ASTM International, West Conshohocken, PA, 2014,
  www.astm.org

