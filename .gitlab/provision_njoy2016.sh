#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

apt-get update

apt-get -y --no-install-recommends install \
  ca-certificates \
  cmake \
  gfortran \
  git \
  make \
  python3

git clone https://github.com/njoy/NJOY2016.git --branch feature/rebased_pjgriff_mods

mkdir -p NJOY2016/build && cd NJOY2016/build
cmake -DCMAKE_INSTALL_PREFIX=/ ..
make -j
make install

# Cleanup NJOY repository and unneeded packages for execution.
cd ../.. && rm -rf NJOY2016
apt -y remove cmake make python3
apt -y autoremove

